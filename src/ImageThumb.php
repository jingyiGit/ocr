<?php

namespace JyOcr;

class ImageThumb extends ImagePixel
{
  public function __construct($imageFile)
  {
    parent::__construct($imageFile);
  }
  
  /**
   * 九宫格模式，取从标
   *
   * @return void
   */
  public function getNinePalaces()
  {
  
  }
  
  /**
   * 铜钱模式，取坐标
   *
   * @return array[]
   */
  public function getCopper()
  {
    // 取铜钱模式的4个起始点
    $getColorValue = function () {
      return [
        'top'    => [
          'x' => intval(($this->width - 5) / 2),
          'y' => 1,
        ],
        'right'  => [
          'x' => $this->width - 5,
          'y' => intval($this->height / 2),
        ],
        'bottom' => [
          'x' => intval(($this->width - 5) / 2),
          'y' => $this->height - 1,
        ],
        'left'   => [
          'x' => 1,
          'y' => intval($this->height / 2),
        ],
      ];
    };
    $colorPosition = $getColorValue();
    
    // 取出4个角，三点的颜色值
    foreach ($colorPosition as $position => $value) {
      for ($i = 1; $i <= 3; $i++) {
        $x                                   = $value['x'] + ($i - 1) * 2;
        $colorPosition[$position]['color'][] = imagecolorat($this->im, $x, $value['y']);
      }
    }
    $colorPosition['info'] = [
      'mode'   => 'copper',
      'width'  => $this->width,
      'height' => $this->height,
    ];
    return $colorPosition;
  }
}
