<?php

namespace JyOcr;

class Ocr extends ImagePixel
{
  private $debug = false;
  private $similarity = 30; // 相似度，0-100，0为百分百一致
  
  public function __construct($imageFile)
  {
    parent::__construct($imageFile);
  }
  
  public function setDebug(bool $value = true)
  {
    $this->debug = $value;
    return $this;
  }
  
  /**
   * 查询颜色
   *
   * @param array $colorPosition
   * @return void
   */
  public function findColor($colorPosition)
  {
    $list     = $this->getAllColorList1();
    $topColor = $colorPosition['top']['color'][0];
    foreach ($list as $k => $color) {
      if ($color == 0) {
        continue;
      }
      
      if ($this->getSimilarity($topColor, $color) < $this->similarity) {
        // dd('成功了' . $k);
        if ($this->similarityCopper($k, $list, $colorPosition)) {
          $this->debug($k, $colorPosition);
          return explode('_', $k);
        }
      }
    }
    return false;
  }
  
  private function debug($position, $colorPosition)
  {
    if (!$this->debug) {
      return;
    }
    // 成功了98_96
    [$x, $y] = explode('_', $position);
    if ($colorPosition['info']['mode'] == 'copper') {
      $draw = new ImageDraw($this->im);
      $y    = $y - 1;
      $x    = $x - ($colorPosition['top']['x'] - $colorPosition['left']['x']);
      $draw->drawRect(
        $x,
        $y,
        $colorPosition['info']['width'],
        $colorPosition['info']['height']
      );
      header("Content-Type: image/jpeg");
      imagejpeg($this->im);
      imagedestroy($this->im);
      exit();
    }
  }
  
  /**
   * 相似度匹配成功，铜钱模式
   *
   * @param string $position      当前找到的颜色位置
   * @param array  $colorList     原始图的颜色列表
   * @param array  $colorPosition 铜钱位置的颜色数据
   * @return void
   */
  private function similarityCopper($position, $colorList, $colorPosition)
  {
    // 成功了98_96
    [$x, $y] = explode('_', $position);
    // 匹配 top 的1-2-3
    for ($i = 1; $i <= 3; $i++) {
      $list[] = [
        'x'     => $x + ($i - 1) * 2,
        'y'     => intval($y),
        'color' => $colorPosition['top']['color'][$i - 1],
      ];
    }
    
    // 匹配 right 的1-2-3
    $startX = $colorPosition['right']['x'] - $colorPosition['top']['x'];
    $startY = $colorPosition['right']['y'] - $colorPosition['top']['y'];
    for ($i = 1; $i <= 3; $i++) {
      $list[] = [
        'x'     => $x + $startX + ($i - 1) * 2,
        'y'     => intval($y) + $startY,
        'color' => $colorPosition['right']['color'][$i - 1],
      ];
    }
    
    // 匹配 bottom 的1-2-3
    $startY = $colorPosition['bottom']['y'] - $colorPosition['top']['y'];
    for ($i = 1; $i <= 3; $i++) {
      $list[] = [
        'x'     => $x + ($i - 1) * 2,
        'y'     => intval($y) + $startY,
        'color' => $colorPosition['bottom']['color'][$i - 1],
      ];
    }
    
    // 匹配 left 的1-2-3
    $startX = $colorPosition['top']['x'] - $colorPosition['left']['x'];
    $startY = $colorPosition['left']['y'] - $colorPosition['top']['y'];
    for ($i = 1; $i <= 3; $i++) {
      $list[] = [
        'x'     => $x - $startX + ($i - 1) * 2,
        'y'     => intval($y) + $startY,
        'color' => $colorPosition['left']['color'][$i - 1],
      ];
    }
    
    $count = 0;
    foreach ($list as $v) {
      // print_r($v);
      $color = $colorList[$v['x'] . '_' . $v['y']];
      if (($fail = $this->getSimilarity($color, $v['color'])) < 200) {
        $count++;
      } else {
        // print_r($fail . '|');
      }
    }
    return $count >= 8;
  }
}
