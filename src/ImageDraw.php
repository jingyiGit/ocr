<?php
// 图像绘制（独立类）
// 绘制一个部分弧并填充 https://www.w3cschool.cn/doc_php/php-function-imagefilledarc.html?lang=en
// 泛光填充 https://www.w3cschool.cn/doc_php/php-function-imagefill.html?lang=en
// 画椭圆 https://www.w3cschool.cn/doc_php/php-function-imageellipse.html?lang=en
// 图像填充椭圆 https://www.w3cschool.cn/doc_php/php-function-imagefilledellipse.html?lang=en
// 图像填充的多边形 https://www.w3cschool.cn/doc_php/php-function-imagefilledpolygon.html?lang=en

namespace JyOcr;

class ImageDraw extends ImageInfo
{
  /**
   * 画矩形
   *
   * @param int $x      起始x坐标
   * @param int $y      起始y坐标
   * @param int $width  矩形宽度
   * @param int $height 矩形调试
   * @param int $color  矩形边框颜色，10进制颜色值
   * @return $this
   */
  public function drawRect(int $x, int $y, int $width, int $height, int $color = 0)
  {
    $rgb = $this->getRGB($color);
    $red = imagecolorallocate($this->im, $rgb['r'], $rgb['g'], $rgb['b']);
    imagerectangle($this->im, $x, $y, $x + $width, $y + $height, $red);
    return $this;
  }
  
  /**
   * 画矩形并填充
   *
   * @param int $x      起始x坐标
   * @param int $y      起始y坐标
   * @param int $width  矩形宽度
   * @param int $height 矩形调试
   * @param int $color  填充颜色，10进制颜色值
   * @return $this
   */
  public function drawRectFill(int $x, int $y, int $width, int $height, int $color = 0)
  {
    $rgb = $this->getRGB($color);
    $red = imagecolorallocate($this->im, $rgb['r'], $rgb['g'], $rgb['b']);
    imagefilledrectangle($this->im, $x, $y, $x + $width, $y + $height, $red);
    return $this;
  }
  
  /**
   * 输出 / 保存
   *
   * @param string|null $fileName 留空为输出到浏览器，为文件名时，将保存到磁盘
   * @return void
   */
  public function output($fileName = null)
  {
    if (!$exists = file_exists($fileName)) {
      header("Content-Type: image/jpeg");
    }
    imagejpeg($this->im, $fileName);
    imagedestroy($this->im);
  }
  
  private function getRGB($color)
  {
    return [
      'b' => $color & 0xFF,          // b
      'g' => ($color >> 8) & 0xFF,   // g
      'r' => ($color >> 16) & 0xFF,  // r
    ];
  }
}
